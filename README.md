# Blue Marlin

Bluemarlin was created during 24h hackaton at Docker HQ by two developers (one of them is not an JS developer :))

This software is provided "as is" and it's not maintained.

## CLI

Usage:

    cli/bluemarlin.js --help

## License

The MIT License (MIT)

Full license text in *LICENSE* file
