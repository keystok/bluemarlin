#:nodoc:
class HelloController < ApplicationController
  def index
    @secure_string = Rails.application.secrets.secret_string
  end
end
