BluemarlinView = require './bluemarlin-view'

module.exports =
  bluemarlinView: null

  activate: (state) ->
    @bluemarlinView = new BluemarlinView(state.bluemarlinViewState)

  deactivate: ->
    @bluemarlinView.destroy()

  serialize: ->
    bluemarlinViewState: @bluemarlinView.serialize()
