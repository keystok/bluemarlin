var fs = require('fs');
var path = require('path');

var runLines = {
  'postgresql':
    'RUN apt-get install -qq -y postgresql postgresql-server-dev-all\n' +
    "RUN sed -e 's/\\(host *all *all *127\\.0\\.0\\.1\\/32 *\\)md5/\\1trust/' -i /etc/postgresql/9.1/main/pg_hba.conf\n" +
    'RUN /etc/init.d/postgresql start && su -l postgres -c "psql -c \\"UPDATE pg_database SET datistemplate = FALSE WHERE datname = \'template1\';\\"" && su -l postgres -c "psql -c \\"DROP DATABASE template1;\\"" && su -l postgres -c "psql -c \\"CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = \'UNICODE\';\\"" && su -l postgres -c "psql -c \\"UPDATE pg_database SET datistemplate = TRUE WHERE datname = \'template1\';\\"" && su -l postgres -c "psql -c \\" VACUUM FREEZE;\\""; su -l postgres -c "createuser -s root; createdb -O root root"\n',
  'ruby':
    'RUN apt-get install -qq -y curl nodejs patch bzip2 gawk g++ make patch libreadline6-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev\n' +
    'RUN \curl -sSL https://get.rvm.io | bash -s stable --ruby=%VERSION%\n' +
    'RUN useradd -m -G rvm -s /bin/bash app\n' +
    'RUN bash -l -c "gem install keystok"\n' +
    'RUN bash -l -c "gem install passenger"\n',
  'rails':
    'ADD ./Gemfile /app/Gemfile\n' +
    'ADD ./Gemfile.lock /app/Gemfile.lock\n' +
    'RUN cd /app; bash -l -c "bundle install"\n' +
    'ADD . /app\n'
};

var cmdLines = {
  'postgresql':
    '/etc/init.d/postgresql start',
  'rails':
    'bash -l -c "ruby lib/keystok_fix_me.rb . && rake db:create db:migrate && passenger start"'
};

// Create a Dockerfile in the specified path, using the specified config
// Config is an object containing dependencies
module.exports = function(projectPath, repository, overwrite, config, callback) {
  var dockerfilePath = path.join(projectPath, 'Dockerfile');
  fs.stat(dockerfilePath, function (err, stat) {
    if (!err && !overwrite) {
      callback('alreadyexists');
    } else {
      // We can write the file now
      if (!config.dependencies.ruby) config.dependencies.ruby = '2.1.2';
      if (!config.dependencies.rails) config.dependencies.rails = '4.1.0';
      //if (!config.name) config.name = 'bluemarlin-test';
      var contents = '';
      contents += '#BMNAME ' + (repository ? repository + '/' : '') + config.info.name.toLowerCase() + '\n';
      contents += 'FROM ubuntu:12.04\n';
      contents += 'RUN apt-get update -qq\n';
      for (var dep in runLines) {
        if (config.dependencies.hasOwnProperty(dep)) {
          var line = runLines[dep];
          if (line) {
            var version = config.dependencies[dep];
            contents += line.replace('%VERSION%', version);
          } else {
            console.log('WARNING: Unknown dependency', dep);
          }
        }
      }
      contents += 'WORKDIR /app\n';
      contents += 'EXPOSE 3000\n';
      contents += '#BMPORT 49000\n';
      contents += 'CMD';
      var first = true;
      for (var dep in cmdLines) {
        var cmd = cmdLines[dep];
        if (cmd) {
          if (first) {
            first = false;
            contents += ' ';
          } else {
            contents += '; ';
          }
          contents += cmd;
        }
      }
      contents += '\n';
      fs.writeFile(dockerfilePath, contents, function (err) {
        callback(err);
      });
    }
  });
};
