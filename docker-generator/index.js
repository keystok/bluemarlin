var fs = require('fs');
var path = require('path');
var dockerfileWriter = require('./dockerfile-writer');
var RailsDetector = require('bluemarlin-rails-detector');

// Autoconvert Rails project to Dockerfile
module.exports = function(railsPath, repository, overwrite, callback) {
  var gemfilePath = path.join(railsPath, 'Gemfile');
  fs.stat(gemfilePath, function (err, stats) {
    if (err) {
      console.log('Cannot find Gemfile:', err);
    } else {
      console.log('Converting Rails project in', gemfilePath);
      var detector = new RailsDetector();
      detector.detectServices(railsPath, function (err, config) {
        if (err) {
          console.log('Error reading Rails project:', err);
        } else {
          // Generate Docker project
          dockerfileWriter(railsPath, repository, overwrite, config, function (err) {
            if (err) {
              console.log('Error generating Docker project:', err);
            } else {
              console.log('Project generated!');
            }
            callback(err);
          });
        }
      });
    }
  });
};
