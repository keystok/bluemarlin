fs = require('fs');
path = require('path');
_ = require('underscore');

var RailsDetector = function(options) {
  this.knownServices = {
    'pg': 'postgresql',
    'mysql': 'mysql',
    'rails': 'rails',
    'redis': 'redis',
    'hiredis': 'hiredis'
  };
};

RailsDetector.prototype.detectServices = function(folderPath, callback) {
  var that = this;
  fs.readdir(folderPath, function (err, files) {
    if(err) {
      throw(err);
    } else {
      that.scanForGemfile(files, {'folderPath': folderPath, 
                                  'callback': callback,
                                  'projectInfo': {'info': {}, 'dependencies': {}}});
    }
  });
}

RailsDetector.prototype.detectGemfile = function(gemfilePath, options) {
  var that = this;
  var content = fs.readFile(gemfilePath, function(err, content) {
    if(err) {
      throw(err);
    } else {
      that.detectGemfileContent(content.toString(), options);
      that.scanForProjectName(options);
    }
  });
}

RailsDetector.prototype.scanForProjectName = function(options) {
  fs.readFile(path.join(options.folderPath, 'config', 'application.rb'), function(err, content) {
    if(err) {
      throw(err);
    } else {
      nameMatcher = content.toString().match(/module (.*)/);
      options.projectInfo.info.name = nameMatcher[1];
      options.callback(null, options.projectInfo);
    }
  });
}

RailsDetector.prototype.detectGemfileContent = function(content, options) {
  for (service in this.knownServices) {
    var regexp = new RegExp(' {4}' + service + ' \\(([a-z0-9.]+)\\)'); 
    serviceCheck = content.match(regexp);
    if(serviceCheck) {
      options.projectInfo.dependencies[this.knownServices[service]] = serviceCheck[1];
    }
  }
}

RailsDetector.prototype.scanForGemfile = function(files, options) {
  var gemfile = _.find(files, function (fileName) {
    return fileName == 'Gemfile.lock';
  });
  if (!gemfile) {
    throw(new Error('Gemfile.lock not found'));
  }
  this.detectGemfile(path.join(options.folderPath, gemfile), options);
}

module.exports = RailsDetector

// detector = new RailsDetector();
// detector.detectServices('../sample-project/rails/postgresql', function(err, services) {
//   console.log(services);
// });