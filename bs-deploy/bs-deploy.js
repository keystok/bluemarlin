var AWS = require('aws-sdk');
var childProcess = require('child_process')
var fs = require('fs-extra');
var keystokSDK = require('keystok');
var temp = require('temp');
var path = require('path');
var _ = require('underscore');

var BSDeploy = function(options) {
  this.options = options;
  this.keystok = keystokSDK(options.keystokAccessToken);
  this.environmentName = options.environmentName;
  this.log = options.log || console.log;
};

BSDeploy.prototype.deploy = function(options, callback) {
  var that = this;
  var keys = ['access_key_id',
              'secret_access_key',
              'aws_region',
              'instance_type',
              'key_pair'];
  this.keystok.get(keys, function(err, keys) {
    AWS.config.update({accessKeyId: keys.access_key_id, secretAccessKey: keys.secret_access_key});
    that.options.keys = keys;
    that.createApplication(keys, options, callback);
  });
};

BSDeploy.prototype.createApplication = function(keys, options, callback) {
  var that = this;
  temp.track();
  temp.open('blue-marlin-docker-zip', function(err, info) {
    fs.copy(path.join(__dirname, '..', 'support/keystok_fix_me.rb'), path.join(that.options.sourcePath, 'lib', 'keystok_fix_me.rb'), function(err) {
      childProcess.child = childProcess.exec('zip -r ' + info.path + ' *', {cwd: that.options.sourcePath}, function (error, stdout, stderr) {
        that.log('ZIP created');
        that.options.zip_path = info.path + '.zip'
        that.checkS3Bucket(options, callback);
      });
    });
  });
};

BSDeploy.prototype.checkS3Bucket = function(options, callback) {
  var that = this;
  var s3 = new AWS.S3({region: that.options.keys.aws_region});
  that.options.s3 = s3;
  that.log('Checking S3 bucket existence');
  s3.headBucket({'Bucket': that.options.bucketName}, function(err, data) {
    if (err) {
      that.createS3Bucket(options, callback);
    }
    else {
      that.uploadZip(options, callback);
    }
  });
}

BSDeploy.prototype.createS3Bucket = function(options, callback) {
  var that = this;
  this.log('Creating S3 bucket');
  this.options.s3.createBucket({'Bucket': this.options.bucketName}, function(err, data) {
    if(err) {
      throw err;
    } else {
      that.uploadZip(options, callback);
    }
  });
}

BSDeploy.prototype.uploadZip = function(options, callback) {
  var that = this;
  that.log('ZIP upload');
  fs.readFile(this.options.zip_path, function(err, data) {
    if(err) {
      throw(err);
    }
    var keyName = 'app-' + new Date().toISOString() + '.zip'
    that.options.keyName = keyName;
    that.options.s3.putObject({'Bucket': that.options.bucketName,
                               'Key': keyName,
                               'Body': data}, function(err, data) {
      if(err) {
        throw err;
      } else {
        that.checkBSApplication(options, callback);
      }
    });
  });
}

BSDeploy.prototype.checkBSApplication = function(options, callback) {
  var that = this;
  that.log('Checking Application existence');
  that.options.bs = new AWS.ElasticBeanstalk({'region': that.options.keys.aws_region});
  that.options.bs.describeApplications({'ApplicationNames': [that.options.appName]}, function(err, data) {
    if (err) {
      throw err;
    } else {
      if (data.Applications.length == 0) {
        that.createBSApplication(options, callback);
      } else {
        that.createBSVersion(options, callback);
      }
    }
  });
}

BSDeploy.prototype.createBSApplication = function(options, callback) {
  var that = this;
  that.log('Creating Application');
  that.options.bs.createApplication({'ApplicationName': that.options.appName}, function(err, data) {
    if (err) {
      throw err;
    } else {
      that.createBSVersion(options, callback);
    }
  });
}

BSDeploy.prototype.checkBSEnvironment = function(options, callback) {
  var that = this;
  that.log('Checking Environment existence');
  that.options.bs.describeEnvironments({'ApplicationName': that.options.appName,
                                        'EnvironmentNames': [that.environmentName]}, function(err, data) {
    if (err) {
      throw err;
    } else {
      if (data.Environments.length == 0 || data.Environments[0].Status == 'Terminated') {
        that.findSolutionStack(options, callback);
      } else {
        that.updateBSEnvironment(options, callback);
      }
    }
  });
}

BSDeploy.prototype.findSolutionStack = function(options, callback) {
  var that = this;
  that.log('Finding solution stack');
  that.options.bs.listAvailableSolutionStacks({}, function(err, data) {
    if (err) {
      throw(err);
    } else {
      that.options.stackName = _.find(data.SolutionStacks, function(stack) {
        return stack.match(/running Docker/)
      });
      if (!that.options.stackName) {
        throw('Stack name not found');
      }
      that.createBSEnvironment(that.options, callback);
    }
  });
}

BSDeploy.prototype.createBSEnvironment = function(options, callback) {
  var that = this;
  that.log('Creating Environment');
  that.options.bs.createEnvironment({'ApplicationName': that.options.appName,
                                     'EnvironmentName': that.environmentName,
                                     'SolutionStackName': that.options.stackName,
                                     'VersionLabel': that.options.keyName,
                                     'OptionSettings': [
                                       { "Namespace": "aws:autoscaling:launchconfiguration",
                                         "OptionName": "InstanceType",
                                         "Value": that.options.keys.instance_type },
                                       { "Namespace": "aws:autoscaling:launchconfiguration",
                                         "OptionName": "EC2KeyName",
                                         "Value": that.options.keys.key_pair },
                                       { "Namespace": "aws:elasticbeanstalk:application:environment",
                                         "OptionName": "KEYSTOK_TOKEN",
                                         "Value": that.options.keystokAccessToken
                                       }
                                     ]}, function(err, data) {
    if (err) {
      throw(err);
    } else {
      callback();
    }
  });
}

BSDeploy.prototype.updateBSEnvironment = function(options, callback) {
  var that = this;
  that.log('Updating Environment');
  that.options.bs.updateEnvironment({'EnvironmentName': that.environmentName,
                                     'VersionLabel': that.options.keyName,
                                     'OptionSettings': [
                                       { "Namespace": "aws:autoscaling:launchconfiguration",
                                         "OptionName": "InstanceType",
                                         "Value": that.options.keys.instance_type},
                                       { "Namespace": "aws:autoscaling:launchconfiguration",
                                         "OptionName": "EC2KeyName",
                                         "Value": that.options.keys.key_pair },
                                       { "Namespace": "aws:elasticbeanstalk:application:environment",
                                         "OptionName": "KEYSTOK_TOKEN",
                                         "Value": that.options.keystokAccessToken
                                       }
                                     ]}, function(err, data) {
    if (err) {
      throw(err);
    } else {
      callback();
    }
  });
}

BSDeploy.prototype.createBSVersion = function(options, callback) {
  var that = this;
  that.log('Creating Version');
  that.options.bs.createApplicationVersion({'ApplicationName': that.options.appName,
                                            'VersionLabel': that.options.keyName,
                                             SourceBundle: {
                                               S3Bucket: that.options.bucketName,
                                               S3Key: that.options.keyName,
                                             }}, function(err, data) {
    if (err) {
      throw(err);
    } else {
      that.checkBSEnvironment(options, callback);
    }
  });
}

module.exports = BSDeploy;

// var bs = new BSDeploy({'keystokAccessToken': 'eyJpZCI6ODIsInJ0IjoiMzE0ZjVkZWNmNjRhMGQ4YTZjMjJjNThlNjAxNTc4MzNjYzA2YmZiYzczNzc5OTYxMzQ5NmNjNWRjNTA0ZDAzYyIsImRrIjoiYzNkYTVkNTA0YWI3ZGI2ZGQ0ZmZlZjBiMjNmZmJiMDcxYjhkOGQwZmRkMGQzNWNiNmQ1MGM1MjgyNWNlOWE4NSJ9',
//                        'sourcePath': '../sample-project/rails/postgresql/',
//                         'appName': 'TestBSApp',
//                         'bucketName': 'BlueMarlinKeystok'});
// bs.deploy({}, function(err) {console.log('Deployed!')});
