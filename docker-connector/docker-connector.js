var fs = require('fs-extra');
var temp = require('temp');
var path = require('path');
var Docker = require('dockerode');
var childProcess = require('child_process')

var DockerConnector = function(options) {
  this.options = options;
  this.docker = new Docker(this.options);

  // Monkey-patch Docker to support container names
  Docker.prototype.createContainer = function(opts, callback) {
    var self = this;
    var name = opts.name;
    delete opts.name;
    var optsf = {
      path: '/containers/create?name=' + name + '&',
      method: 'POST',
      options: opts,
      statusCodes: {
        201: true,
        404: "no such container",
        406: 'impossible to attach',
        500: "server error"
      }
    };

    this.modem.dial(optsf, function(err, data) {
      if(err) return callback(err, data);
      callback(err, self.getContainer(data.Id));
    });
  };
};

DockerConnector.prototype.buildImage = function(sourcePath, imageName, callback) {
  var that = this;
  temp.track();
  temp.open('blue-marlin-docker', function(err, info) {
    fs.copy(path.join(__dirname, '..', 'support/keystok_fix_me.rb'), path.join(sourcePath, 'lib', 'keystok_fix_me.rb'), function(err) {
      if(err) {
        throw(err);
      }
      childProcess.child = childProcess.exec('tar cvf ' + info.path + ' *', {cwd: sourcePath}, function (error, stdout, stderr) {
        that.buildDockerImage({'tarball_path': info.path,
                               'imageName': imageName,
                               'callback': callback});
      });
    });
  });
};

DockerConnector.prototype.buildDockerImage = function(options) {
  var that = this;
  that.docker.buildImage(options.tarball_path, {t: options.imageName}, function (err, response){
    options.callback(err, response);
  });
};

DockerConnector.prototype.listContainers = function(callback) {
  var that = this;
  that.docker.listContainers(callback);
};

DockerConnector.prototype.listImages = function(callback) {
  var that = this;
  that.docker.listImages(callback);
};

DockerConnector.prototype.imageExists = function(imageName, callback) {
  var that = this;
  that.docker.listImages(function (err, images) {
    if (err) {
      return callback(err);
    }
    var i, j, image;
    for (i = 0; i < images.length; i++) {
      image = images[i];
      for (j = 0; j < image.RepoTags.length; j++) {
        if (image.RepoTags[j] == imageName) {
          return callback(null);
        }
      }
    }
    callback('notfound');
  });
};

DockerConnector.prototype.containerExists = function(containerName, callback) {
  var that = this;
  that.docker.listContainers(function (err, containers) {
    if (err) {
      return callback(err);
    }
    var i, j, container;
    for (i = 0; i < containers.length; i++) {
      container = containers[i];
      for (j = 0; j < container.Names.length; j++) {
        if (container.Names[j] == '/' + containerName) {
          return callback(null);
        }
      }
    }
    callback('notfound');
  });
};

DockerConnector.prototype.getContainerByName = function(name, callback) {
var that = this;
that.docker.listContainers(function (err, containers) {
  if (err) {
    return callback(err);
  }
  var i, j, container;
  for (i = 0; i < containers.length; i++) {
    container = containers[i];
    for (j = 0; j < container.Names.length; j++) {
      if (container.Names[j] == '/' + name) {
        return that.getContainer(container.Id, callback);
      }
    }
  }
  callback('notfound');
});
};

DockerConnector.prototype.getContainer = function(id, callback) {
  var that = this;
  var id = that.docker.getContainer(id);
  if (callback) {
    callback(null, id);
  }
  return id;
};

DockerConnector.prototype.getImage = function(id, callback) {
  var that = this;
  return that.docker.getImage(id, callback);
};

DockerConnector.prototype.pushImage = function(imageName, callback) {
  var that = this;
  var image = that.getImage(imageName);
  fs.readFile(path.join(process.env.HOME, '.dockercfg'), {encoding:'utf8'}, function(err, data) {
    if (err) {
      return callback(err);
    }
    try {
      var auth = JSON.parse(data);
      auth = auth['https://index.docker.io/v1/'];
    } catch (err) {
      return callback(err);
    }
    auth = JSON.stringify({
      username: 'kennu',
      password: new Buffer(auth.auth + '====', 'base64').toString('utf8'),
      email: auth.email,
      serveraddress: 'https://index.docker.io/v1/'
    });
    auth = new Buffer(auth).toString('base64');
    return image.push({}, callback, auth);
  });
}

DockerConnector.prototype.runContainer = function(containerName, imageName, publicPort, privatePort, environment, callback) {
  var that = this;
  /*
  that.docker.run(imageName, null, [process.stdout, process.stderr], {'Names':[containerName]}, function (err, data, container) {
    callback(err, data, container);
  });
  */
  var ExposedPorts = {};
  var PortBindings = {};
  ExposedPorts[privatePort + '/tcp'] = {};
  PortBindings[privatePort + '/tcp'] = [{HostPort:publicPort.toString()}];
  that.docker.createContainer({
    name: containerName,
    Image: imageName,
    Tty: false,
    ExposedPorts: ExposedPorts,
    Env: environment
  }, function (err, container) {
    if (err) {
      callback(err);
    } else {
      container.start({PortBindings:PortBindings}, function (err, data) {
        callback(err, data);
      });
    }
  });
};

module.exports = DockerConnector;

// var dk = new DockerConnector({socketPath: '/var/run/docker.sock'});
// dk.buildImage('/home/sokol/Dane/Keystok/git/bluemarlin/sample-project/rails/postgresql', 'test0004', function(err, response) {
//   console.log(err, response);
// });
